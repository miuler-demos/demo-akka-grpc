package miuler.demo.akka.grpc
import java.util.concurrent.CompletableFuture

import akka.grpc.GrpcClientSettings

import scala.concurrent.Future

class GrpcServer1Imp extends GrpcServer1 {
  override def getCard(card1Request: Card1Request): Future[Card1Response] = {
//    val demoServiceGrpcClientSettings = GrpcClientSettings.fromConfig(GrpcServer2.name)
//    Card1Response(cardNumber, fullName)

    val offers = List(
      Offer1("100", "Valor de la oferta"),
      Offer1("200", "Valor de la oferta"),
      Offer1("300", "Valor de la oferta")
    )
    val card1Response = Card1Response(card1Request.cardNumber, "Miuler", offers)
    Future.successful(card1Response)
  }
}
