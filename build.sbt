
Global / scalaVersion := "2.13.0"

initialize := {
  val _ = initialize.value
  if (sys.props("java.specification.version") != "1.8")
    sys.error("Java 8 is required for this project.")
}


lazy val root = (project in file("."))
  .aggregate(grpc_server_1, grpc_server_2, grpc_server_3, grpc_server_4)

lazy val grpc_server_1 = project
  .enablePlugins(AkkaGrpcPlugin, JavaAgent)
  .settings(
    javaAgents += "org.mortbay.jetty.alpn" % "jetty-alpn-agent" % "2.0.9" % "runtime;test"
  )

lazy val grpc_server_2 = project
  .enablePlugins(AkkaGrpcPlugin, JavaAgent)
  .settings(
    javaAgents += "org.mortbay.jetty.alpn" % "jetty-alpn-agent" % "2.0.9" % "runtime;test"
  )

lazy val grpc_server_3 = project
  .enablePlugins(AkkaGrpcPlugin, JavaAgent)
  .settings(
    javaAgents += "org.mortbay.jetty.alpn" % "jetty-alpn-agent" % "2.0.9" % "runtime;test"
  )

lazy val grpc_server_4 = project
  .enablePlugins(AkkaGrpcPlugin, JavaAgent)
  .settings(
    javaAgents += "org.mortbay.jetty.alpn" % "jetty-alpn-agent" % "2.0.9" % "runtime;test"
  )




