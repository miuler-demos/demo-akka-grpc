package miuler.demo.akka.grpc

import akka.actor.ActorSystem
import akka.grpc.scaladsl.ServiceHandler
import akka.http.scaladsl.{Http, HttpConnectionContext}
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.ConfigFactory

import scala.concurrent.{ExecutionContext, Future}

object Grpc2Application {

  def main(args: Array[String]): Unit = {
    val conf = ConfigFactory
      .parseString("akka.http.server.preview.enable-http2 = on")
      .withFallback(ConfigFactory.defaultApplication())
    val system = ActorSystem("DemoGrpc", conf)
    new Grpc2Application(system).run()
  }

}

class Grpc2Application(system: ActorSystem) {
  def run(): Future[Http.ServerBinding] = {
    implicit val sys: ActorSystem = system
    implicit val mat: Materializer = ActorMaterializer()
    implicit val ec: ExecutionContext = sys.dispatcher

    val service: HttpRequest => Future[HttpResponse] = GrpcServer2Handler(new GrpcServer2Imp())

    val binding = Http().bindAndHandleAsync(
      service,
      "0.0.0.0",
      8080,
      HttpConnectionContext()
    )
    binding.foreach((binding: Http.ServerBinding) => println(s"gRPC server bound to: ${binding.localAddress}"))
    binding
  }

}
