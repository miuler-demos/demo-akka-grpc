package miuler.demo.akka.grpc

import java.util.concurrent.CompletableFuture

import akka.grpc.GrpcClientSettings

import scala.concurrent.Future

class GrpcServer2Imp extends GrpcServer2 {
  override def getCard(cardRequest: CardRequest): Future[CardResponse] = {
    //val demoServiceGrpcClientSettings = GrpcClientSettings.fromConfig(DemoService.name)
    Future.successful(CardResponse(cardRequest.cardNumber, "Miuler"))
  }
}
